=== Custom Facebook Feed Pro - Reviews ===
Author: Smash Balloon
Support Website: http://smashballoon.com/extensions/date-range/
Requires at least: 3.0
Tested up to: 4.9
Version: 1.0.5
License: Non-distributable, Not for resale

Adds the ability to display reviews from your Facebook page.

== Description ==

Adds the ability to display reviews from your Facebook page. Once activated the extension will add an option to your Settings page which allows you to enter your Facebook Page Access Token, and some options to the Customize page which allows you to customize your Reviews feed. You can create a reviews feed by setting type=reviews in your shortcode.


== Installation ==
1. Install the plugin via the WordPress plugin directory, or by uploading the files to your web server (in the /wp-content/plugins/ directory).
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. Navigate to Settings > License to enter and activate your license
4. Once activated the extension will add an option to your Settings page which allows you to enter your Facebook Page Access Token, and some options to the Customize page which allows you to customize your Reviews feed. You can create a reviews feed by setting type=reviews in your shortcode.

== Changelog ==

1.0.5
* New: Supports recommendations
* Fix: Now displays the avatars of people leaving reviews
* Tweak: Replaced link to the reviewers profile with link to the reviews page instead

1.0.4
* New: Added the ability to filter reviews by word/phrase. Just use the `filter` and `exfilter` shortcode settings.
* Tweak: Increased size of Reviews Access Token field so entire token is visible
* Fix: Updated directions in the tooltip on how to get a Reviews Access Token
* Fix: Removed broken image if avatar not available

1.0.3
* Fix: The wrong number of posts would sometimes be displayed when only showing reviews with certain star ratings or when choosing to only show reviews that contain text
* Fix: The `offset` shortcode option wasn't working correctly with reviews
* Fix: Fixed an issue where the text length setting wouldn't be applied to the review text
* Tweak: Updated the plugin updater script to reduce requests on the WordPress Plugins page

= 1.0.2 =
* New: Added a setting that allows you to hide reviews that don't include any text

= 1.0.1 =
* Fix: Fixed an issue where line breaks in the review text was being converted to HTML line break tags
* Fix: Fixed an issue where 1 star checkbox on the settings page wouldn't stay enabled

= 1.0 =
* Launched the Reviews extension