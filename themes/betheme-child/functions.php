<?php

/* ---------------------------------------------------------------------------
 * Child Theme URI | DO NOT CHANGE
 * ----------------------------------- ---------------------------------------- */
define( 'CHILD_THEME_URI', get_stylesheet_directory_uri() );


/* ---------------------------------------------------------------------------
 * Define | YOU CAN CHANGE THESE
 * --------------------------------------------------------------------------- */

// White Label --------------------------------------------
define( 'WHITE_LABEL', false );

// Static CSS is placed in Child Theme directory ----------
define( 'STATIC_IN_CHILD', false );


/* ---------------------------------------------------------------------------
 * Enqueue Style
 * --------------------------------------------------------------------------- */
add_action( 'wp_enqueue_scripts', 'mfnch_enqueue_styles', 101 );
function mfnch_enqueue_styles() {
	
	// Enqueue the parent stylesheet
// 	wp_enqueue_style( 'parent-style', get_template_directory_uri() .'/style.css' );		//we don't need this if it's empty
	
	// Enqueue the parent rtl stylesheet
	if ( is_rtl() ) {
		wp_enqueue_style( 'mfn-rtl', get_template_directory_uri() . '/rtl.css' );
	}
	
	// Enqueue the child stylesheet
	wp_dequeue_style( 'style' );
	wp_enqueue_style( 'style', get_stylesheet_directory_uri() .'/style.css' );
	
}


/* ---------------------------------------------------------------------------
 * Load Textdomain
 * --------------------------------------------------------------------------- */
add_action( 'after_setup_theme', 'mfnch_textdomain' );
function mfnch_textdomain() {
    load_child_theme_textdomain( 'betheme',  get_stylesheet_directory() . '/languages' );
    load_child_theme_textdomain( 'mfn-opts', get_stylesheet_directory() . '/languages' );
}


/* ---------------------------------------------------------------------------
 * Override theme functions
 * 
 * if you want to override theme functions use the example below
 * --------------------------------------------------------------------------- */
 
 
 /*
 * EXAMPLE OF CHANGING ANY TEXT (STRING) IN THE EVENTS CALENDAR
 * See the codex to learn more about WP text domains:
 * http://codex.wordpress.org/Translating_WordPress#Localization_Technology
 * Example Tribe domains: 'tribe-events-calendar', 'tribe-events-calendar-pro'...
 */
function tribe_custom_theme_text ( $translation, $text, $domain ) {
 	// Put your custom text here in a key => value pair
	// Example: 'Text you want to change' => 'This is what it will be changed to'
	// The text you want to change is the key, and it is case-sensitive
	// The text you want to change it to is the value
	// You can freely add or remove key => values, but make sure to separate them with a comma
	// This example changes the label "Venue" to "Location", and "Related Events" to "Similar Events"
	$custom_text = array(
		'Your RSVP has been received! Check your email for your RSVP confirmation.' => 
		'Your RSVP has been received! Check your email for your RSVP confirmation. Sign up again to add another person.',
	);
 
	// If this text domain starts with "tribe-", "the-events-", or "event-" and we have replacement text
    	if( (strpos($domain, 'tribe-') === 0 || strpos($domain, 'the-events-') === 0 || strpos($domain, 'event-') === 0) && array_key_exists($translation, $custom_text) ) {
		$translation = $custom_text[$translation];
	}
    return $translation;
}
add_filter('gettext', 'tribe_custom_theme_text', 20, 3);
 
 
// RISA
// Single venue =» Meeting Point
add_filter( 'tribe_venue_label_singular', 'change_single_venue_label' );
function change_single_venue_label() {
    return 'Meeting Point';
}
add_filter( 'tribe_venue_label_singular_lowercase', 'change_single_venue_label_lowercase' );
function change_single_venue_label_lowercase() {
    return 'meeting point';
}
 
// Plural venue =» Meeting Point
add_filter( 'tribe_venue_label_plural', 'change_plural_venue_label' );
function change_plural_venue_label() {
    return 'Meeting Points';
}
add_filter( 'tribe_venue_label_plural_lowercase', 'change_plural_venue_label_lowercase' );
function change_plural_venue_label_lowercase() {
    return 'meeting points';
}

// RISA
/*
 * Hide end time in list, map, photo, and single event view
 * NOTE: This will only hide the end time for events that end on the same day
 */
function tribe_remove_end_time_single( $formatting_details ) {
	$formatting_details['show_end_time'] = 0;
	return $formatting_details;
}
add_filter( 'tribe_events_event_schedule_details_formatting', 'tribe_remove_end_time_single', 10, 2);
/*
 * Hide end time in Week and Month View Tooltips
 * NOTE: This will hide the end time in tooltips for ALL events, not just events that end on the same day
 */
function tribe_remove_end_time_tooltips( $json_array, $event, $additional ) {
	$json_array['endTime'] = '';
	return $json_array;
}
add_filter( 'tribe_events_template_data_array', 'tribe_remove_end_time_tooltips', 10, 3 );
/*
 * Hide endtime for multiday events
 * Note: You will need to uncomment this for it to work
 */
function tribe_remove_endtime_multiday ( $inner, $event ) {
	if ( tribe_event_is_multiday( $event ) && ! tribe_event_is_all_day( $event ) ) {
		$format                   = tribe_get_date_format( true );
		$time_format              = get_option( 'time_format' );
		$format2ndday             = apply_filters( 'tribe_format_second_date_in_range', $format, $event );
		$datetime_separator       = tribe_get_option( 'dateTimeSeparator', ' @ ' );
		$time_range_separator     = tribe_get_option( 'timeRangeSeparator', ' - ' );
		$microformatStartFormat   = tribe_get_start_date( $event, false, 'Y-m-dTh:i' );
		$microformatEndFormat     = tribe_get_end_date( $event, false, 'Y-m-dTh:i' );
		$inner = '<span class="date-start dtstart">';
		$inner .= tribe_get_start_date( $event, false, $format ) . $datetime_separator . tribe_get_start_date( $event, false, $time_format );
		$inner .= '<span class="value-title" title="' . $microformatStartFormat . '"></span>';
		$inner .= '</span>' . $time_range_separator;
		$inner .= '<span class="date-end dtend">';
		$inner .= tribe_get_end_date( $event, false, $format2ndday );
		$inner .= '<span class="value-title" title="' . $microformatEndFormat . '"></span>';
		$inner .= '</span>';
	}
	return $inner;
}
//add_filter( 'tribe_events_event_schedule_details_inner', 'tribe_remove_endtime_multiday', 10, 3 );

// RISA
// limit the number of events per page.
/**
 * Set the events per page on photo view.
 */
function tribe_photo_posts_per_page( $value, $name, $default){
	if ( 'postsPerPage' === $name && tribe_is_photo() && is_front_page()) {
		$value = '3';
	}
	return $value;
}
add_filter( 'tribe_get_option', 'tribe_photo_posts_per_page', 10, 3, 'postsPerPage' );


// RISA 
// Removing some html "Warning: The type attribute is unnecessary for JavaScript resources."
add_filter('script_loader_tag', 'clean_script_tag');
  function clean_script_tag($input) {
  $input = str_replace("type='text/javascript' ", '', $input);		// works
  // $input = str_replace("type='text/javascript'>", '>', $input);	// doesn't work
  return str_replace("'", '"', $input);  // não percebi bem esta linha.
}

// RISA 
// Removing some html "Warning: The type attribute for the style element is not needed and should be omitted."
add_filter('css_loader_tag', 'clean_css_tag');
  function clean_css_tag($input) {
  $input = str_replace("type='text/css'>", '>', $input); // não funciona, não sei porquê
  return str_replace("'", '"', $input);  // não percebi bem esta linha.
}


// RISA 
// REST
add_filter( 'tribe_rest_event_data', 'add_extrainfo_to_response' );
function add_extrainfo_to_response( array $event_data ) {
    $event_id = $event_data['id'];
    $costum_data = get_post_custom( $event_id );

	$total_cost = $costum_data['_ecp_custom_9'];
    $event_data['cost'] = $total_cost;
	
	$what_to_bring = $costum_data['_ecp_custom_11'];
    $event_data['what_to_bring'] = $what_to_bring;
	
	$website = $costum_data['_ecp_custom_17'];
    $event_data['website'] = $website; 
	
	$zomato = $costum_data['_ecp_custom_19'];
    $event_data['zomato'] = $zomato;
	
	$tripadvisor = $costum_data['_ecp_custom_20'];
    $event_data['tripadvisor'] = $tripadvisor;
	
	$bus_leaves_at = $costum_data['_ecp_custom_21'];
    $event_data['bus_leaves_at'] = $bus_leaves_at;
	
	$train_leaves_at = $costum_data['_ecp_custom_22'];
    $event_data['train_leaves_at'] = $train_leaves_at;
	
	$licence = $costum_data['_ecp_custom_24'];
    $event_data['licence'] = $licence;
	
	$includes = $costum_data['_ecp_custom_25'];
    $event_data['includes'] = $includes;
	
	$location = $costum_data['_ecp_custom_32'];
    $event_data['location'] = $location;
	
	$zapping_card = $costum_data['_ecp_custom_33'];
    $event_data['zapping_card'] = $zapping_card;
	
	$boat_leaves_at = $costum_data['_ecp_custom_37'];
    $event_data['boat_leaves_at'] = $boat_leaves_at;
		
	// $event_data['costum_data'] = $costum_data; // For debug
		
	// gallery images
	$description = $event_data['description'];
	
	$imgContent = get_string_between($description, '<img ', '/>');
	$gallery_images = array();
	while (!empty($imgContent)) {
		$img = get_string_between($imgContent, 'src="', '" ');
		
		array_push($gallery_images, $img );
		$description = clean_up_event_content($description, '<img ', '/>');
		$imgContent = get_string_between($description, '<img ', '/>');
	}
	
	// cleanup description
	$description = clean_up_event_content($description, '<style ', '</style>'); // Assumption: they appear only once
	$description = clean_up_event_content($description, '<div ', '</div>');     // Assumption: they appear only once
		
	$event_data['gallery_images'] = $gallery_images;
	$event_data['description'] = $description;
	
	$ticketed = $event_data['ticketed'];
	$ticket_form_id = "";
	if ($ticketed != false) {
		$tickets_form = Tribe__Tickets__Tickets::get_all_event_tickets( $event_id );
		$ticket_form_id = current($tickets_form[0]);
	}
	$event_data['ticket_form_id'] = $ticket_form_id;
		
    return $event_data;	
}

function clean_up_event_content($description, $start, $end){
	$description_style = get_string_between($description, $start, $end);
	$description = str_replace($start . $description_style . $end,"",$description);
    return $description;
}

function get_string_between($string, $start, $end){
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}

// isto core mas não funciona. O array não é invertido
// function tribe_past_reverse_chronological ($post_object) {
//
//     file_put_contents('RISA-debug1.php', current($post_object));
//     file_put_contents('RISA-debug2.php', current($post_object));
//     $past_ajax = (defined( 'DOING_AJAX' ) && DOING_AJAX && $_REQUEST['tribe_event_display'] === 'past') ? true : false;
//     if(tribe_is_past() || $past_ajax) {
//             $post_object = array_reverse($post_object);
//     }
//     file_put_contents('RISA-debug3.php', current($post_object));
//     file_put_contents('RISA-debug4.php', current($post_object));
//
//     return $post_object;
// }
// add_filter('the_posts', 'tribe_past_reverse_chronological', 10, 3 );
?>

