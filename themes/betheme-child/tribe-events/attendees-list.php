<?php
/**
 * Renders the attendee list for an event
 *
 * Override this template in your own theme by creating a file at:
 *
 *     [your-theme]/tribe-events/attendees-list.php
 *
 * @version 4.3.5
 *
 */
?>

<?php /* vou adicionar isto quando tiver várias pessoas a inscrverem-se pala página. No´início não vale a pena
<div class='tribe-attendees-list-container' align="right"> 	<!-- RISA -->
	<h2 class="tribe-attendees-list-title"><?php esc_html_e( 'Who\'s Attending', 'event-tickets-plus' ) ?></h2>
	<p><?php echo esc_html( sprintf( _n( 'One explorer is attending %2$s', '%d explorers are attending %s', $attendees_total, 'event-tickets-plus' ), $attendees_total, get_the_title( $event->ID ) ) ); ?></p>

	<ul class='tribe-attendees-list'>
	<?php foreach ( $attendees_list as $attendee_id => $avatar_html ) { ?>
		<li class='tribe-attendees-list-item'><?php echo $avatar_html; ?></li>
	<?php } ?>
	</ul>
</div>
*/ ?>