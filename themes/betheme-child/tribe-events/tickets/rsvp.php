<?php
/**
 * This template renders the RSVP ticket form
 *
 * Override this template in your own theme by creating a file at:
 *
 *     [your-theme]/tribe-events/tickets/rsvp.php
 *
 * @version 4.6
 *
 * @var bool $must_login
 */

$is_there_any_product         = false;
$is_there_any_product_to_sell = false;
$are_products_available       = false;

ob_start();
$messages = Tribe__Tickets__RSVP::get_instance()->get_messages();
$messages_class = $messages ? 'tribe-rsvp-message-display' : '';
$now = current_time( 'timestamp' );
$medium_image_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'Medium' );

?>

<form
	id="rsvp-now"
	action=""
	class="tribe-tickets-rsvp cart <?php echo esc_attr( $messages_class ); ?>"
	method="post"
	enctype='multipart/form-data'
>
	<h2 class="tribe-events-tickets-title tribe--rsvp">
		<?php echo esc_html_x( 'Sign up', 'form heading', 'event-tickets' ) ?>  		<!-- RISA --> 
	</h2>

	<div class="tribe-rsvp-messages">
		<?php
		if ( $messages ) {
			foreach ( $messages as $message ) {
				?>
				<div class="tribe-rsvp-message tribe-rsvp-message-<?php echo esc_attr( $message->type ); ?>">
					<?php echo esc_html( $message->message ); ?>
				</div>
				<?php
			}//end foreach
		}//end if
		?>

		<div
			class="tribe-rsvp-message tribe-rsvp-message-error tribe-rsvp-message-confirmation-error" style="display:none;">
			<?php esc_html_e( 'Please fill in the Sign up confirmation all fields.', 'event-tickets' ); ?>
		</div>
	</div>

	<table class="tribe-events-tickets tribe-events-tickets-rsvp tribe-tickets-has-rsvp">
		<?php
		foreach ( $tickets as $ticket ) {
			// if the ticket isn't an RSVP ticket, then let's skip it
			if ( 'Tribe__Tickets__RSVP' !== $ticket->provider_class ) {
				continue;
			}

			if ( ! $ticket->date_in_range( $now ) ) {
				continue;
			}

			$is_there_any_product = true;
			$is_there_any_product_to_sell = $ticket->is_in_stock();
			$remaining = $ticket->remaining();

			if ( $is_there_any_product_to_sell ) {
				$are_products_available = true;
			}

			?>
			<tr>
				<td class="tribe-ticket quantity" data-product-id="<?php echo esc_attr( $ticket->ID ); ?>">
					<input type="hidden" name="product_id[]" value="<?php echo absint( $ticket->ID ); ?>">
					<?php if ( $is_there_any_product_to_sell ) : ?>
					<?php /* RISA */ /*
						<input
							type="number"
							class="tribe-ticket-quantity"
							min="0"
							<?php if ( -1 !== $remaining ) : ?>
								max="<?php echo esc_attr( $remaining ); ?>"
							<?php endif; ?>
							name="quantity_<?php echo absint( $ticket->ID ); ?>"
							value="0"
							<?php disabled( $must_login ); ?>
						>
						*/ ?>
						
						<!-- RISA -->
						<input style="visibility:hidden; height:0px;"
							type="number"
							class="tribe-ticket-quantity"
							min="1"
							max="1"
							name="quantity_<?php echo absint( $ticket->ID ); ?>"
							value="1"
							<?php disabled( $must_login ); ?>
						>						
						
						<?php if ( $ticket->managing_stock() ) : ?>
							<span class="tribe-tickets-remaining">
					<?php echo sprintf( esc_html__( '%1$s place(s) out of %2$s available', 'event-tickets' ), $ticket->remaining(), $ticket->capacity() ); ?>
				</span>
						<?php endif; ?>
					<?php else: ?>
						<span class="tickets_nostock"><?php esc_html_e( 'The event is full!', 'event-tickets' ); ?></span>
					<?php endif; ?>
				</td>
				<td class="tickets_name">
					<?php echo esc_html( $ticket->name ); ?>
				</td>
				
				<td class="tickets_description" colspan="2">
					<?php echo esc_html( ( $ticket->show_description() ? $ticket->description : '' ) ); ?>
				</td>
				
			</tr>
			<?php

			/**
			 * Allows injection of HTML after an RSVP ticket table row
			 *
			 * @var Event ID
			 * @var Tribe__Tickets__Ticket_Object
			 */
			do_action( 'event_tickets_rsvp_after_ticket_row', tribe_events_get_ticket_event( $ticket->id ), $ticket );

		}
		?>

		<?php if ( $are_products_available ) : ?>
			<tr class="tribe-tickets-meta-row">
				<td colspan="4" class="tribe-tickets-attendees">
					<header><?php esc_html_e( 'Fill up all fields - Sign up one person at a time', 'event-tickets' ); ?></header>
					<?php
					/**
					 * Allows injection of HTML before RSVP ticket confirmation fields
					 *
					 * @var array of Tribe__Tickets__Ticket_Object
					 */
					do_action( 'event_tickets_rsvp_before_confirmation_fields', $tickets );
					?>
					<table class="tribe-tickets-table">
						<tr class="tribe-tickets-full-name-row">
							<td>
								<label for="tribe-tickets-full-name"><?php esc_html_e( 'First and last names', 'event-tickets' ); ?>:</label>
							</td>
							<td colspan="3">
								<input type="text" name="attendee[full_name]" id="tribe-tickets-full-name" style="margin-bottom: 0px;">
							</td> 
							<td class="td-tickets-form-image" rowspan="3">  <!-- RISA	-->
								<img class="tickets-form-image" src="<?php   echo $medium_image_src[0];?>" alt="ticket image">
							</td> 
						</tr>
						<tr class="tribe-tickets-email-row">
							<td>
								<label for="tribe-tickets-email"><?php esc_html_e( 'Email', 'event-tickets' ); ?>:</label>
							</td>
							<td colspan="3">
								<input type="email" name="attendee[email]" id="tribe-tickets-email" style="margin-bottom: 0px;">
							</td>
						</tr>						
						
						

						<!-- RISA, maybe deleting this code. I dont think that i will need it. -->
						<?php
						/**
						 * Use this filter to hide the Attendees List Optout
						 *
						 * @since 4.5.2
						 *
						 * @param bool
						 */
						$hide_attendee_list_optout = apply_filters( 'tribe_tickets_hide_attendees_list_optout', true ); 	/* true == hide */ 
						if ( ! $hide_attendee_list_optout
							 && class_exists( 'Tribe__Tickets_Plus__Attendees_List' )
							 && ! Tribe__Tickets_Plus__Attendees_List::is_hidden_on( get_the_ID() )
						) : ?>
							<tr class="tribe-tickets-attendees-list-optout">
								<td colspan="4">
									<input
										type="checkbox"
										name="attendee[optout]"
										id="tribe-tickets-attendees-list-optout"
									>
									<label for="tribe-tickets-attendees-list-optout">
										<?php esc_html_e( 'Don\'t list me on the public attendee list', 'event-tickets' ); ?>
									</label>
								</td>
							</tr>
						<?php endif; ?>
						
							<tr class="tribe-tickets-terms-conditions">
								<td colspan="4">
									<input
										type="checkbox"
										name="terms-conditions[optout]"
										id="tribe-tickets-terms-conditions"
									>
									<label style="margin-bottom: 0px;">
									<?php esc_html_e( 'I agree with the ', 'event-tickets' ); ?>
									<a href="../../terms-and-conditions/" target="_blank">terms and conditions</a>
									<?php esc_html_e( ' of this  activity', 'event-tickets' ); ?>
									</label>
								</td>
							</tr>
						
						
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="4" class="add-to-cart">
					<?php if ( $must_login ) : ?>
						<a href="<?php echo esc_url( Tribe__Tickets__Tickets::get_login_url() ); ?>">
							<?php esc_html_e( 'Login to RSVP', 'event-tickets' );?>
						</a>
					<?php else: ?>
						<button
							type="submit"
							name="tickets_process"
							value="1"
							class="tribe-button tribe-button--rsvp"
						>
							<?php esc_html_e( 'Sign up', 'event-tickets' );?>
						</button>
					<?php endif; ?>
				</td>
			</tr>
			<tr style="width:100%;"> <!-- RISA -->
					<td style="width:33%;">  <!-- RISA	-->
						<img class="sun-image" src="//footstepexplorers.com/wp-content/uploads/2018/01/Sun-1.png" alt="ticket image">
					</td> 
					<td style="width:33%;">  <!-- RISA	-->
						<img class="sun-image" src="//footstepexplorers.com/wp-content/uploads/2018/01/Sun-1.png" alt="ticket image">
					</td> 
					<td style="width:33%;" id="sun_off_the_beaten_track">  <!-- RISA	-->
						<img class="sun-image" src="//footstepexplorers.com/wp-content/uploads/2018/01/Sun-1.png" alt="ticket image">
					</td> 
			</tr>
			
		<?php endif; ?>
		<noscript>
			<tr>
				<td class="tribe-link-tickets-message">
					<div class="no-javascript-msg"><?php esc_html_e( 'You must have JavaScript activated to purchase tickets. Please enable JavaScript in your browser.', 'event-tickets' ); ?></div>
				</td>
			</tr>
		</noscript>
	</table>
</form>

<?php
$content = ob_get_clean();
if ( $is_there_any_product ) {
	echo $content;

	// If we have rendered tickets there is generally no need to display a 'tickets unavailable' message
	// for this post
	$this->do_not_show_tickets_unavailable_message();
} else {
	// Indicate that we did not render any tickets, so a 'tickets unavailable' message may be
	// appropriate (depending on whether other ticket providers are active and have a similar
	// result)
	$this->maybe_show_tickets_unavailable_message( $tickets );
}
