
- Versão mobile
		- galeria de fotos da home page escondida
		- barra lateral não carregada em todas as páginas.
- Woocommerce desactivado; não está a ser utilizado;
- imagem de background do header optimizada (.jpeg em vez de .png) ;
- GDPR adicionado à política de privacidade;
- 'Akismet Plugin' desativado porque agora este requer uma chave de API e já não é gratuito para página comerciais;
- Adicionados vários ficheiros que estavam falta na versão 1.4, devido a uma mal utilização do '.gitignore'; Ficheiros 'index.php' e ficheiros que estavam em páginas dentro de pastas 'cache' e 'languages';
- 'event-tickets-plus' desactivado, uma vez que não está a ser utilizado;
- Adicionado o “Classic Editor” Plugin, uma vez que este já não vem instalado com a nova versão do Wordpress (5.0.2);
- WP Super Cache adicionado;

 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

2018-12-29-0053
1.4)
 - 'About Us' com a Mafalda
 - Corrigidas as ligações partidas para a páginas dos organizadores
 - Adicionado:  “extra swetter”, “boat leaves at”
 - Plugins atualizados, excepto os referentes ao 'the events calendar'
 - Recaptcha desativado, uma vez que a versão mais recente (v3) ficava instalada em todas as páginas, o que ficava inestético.
 - Titulo 'coworking' corrigido.
 - 'Confirm rsvp' alterado para 'Confirm sign up'
 - derivativos plugins que não estão a ser utilizados.
 - Theme e plugins carregados no bitbucket. 
 - Betheme-Child a ser carregada via wp-pusher.

 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

2018-07-20_2359
1.3)

 - ssl a funcionar.
 - 'join our community today' com popup com caixa de subscrição da newsletter;
 - Imagem de conteúdo permanente optimizadas. Plugin Smush activado para novos carregamentos de fotos.
 - Adicionadas imagens com passos para carregar o cartão zapping. 
 - “Chamada para acção” no fim da descrição do Zapping (não está graficamente muito bonito);
 - Fotos em falta de pontos de encontro.

Por fazer: 
 - Erro de cache do instagram
 - Optimização de fotos do instagrama e fb.
 - Caches (CDN)
 - Juntar as dezenas de scritps num só
 - Atualizar política de proteção de dados.
 - Reservar passeio.
 - Atualizar versão do php. Não funciona; devo ter algum plugin incompatível com as versões mais recentes. 
 
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

2018-05-09_2306
1.2)
- Fb – artigos e comentários na pg principal;
- Instagram integrado;
- Páginas de conteúdos revistas;
- Inglês revisto;
- Erro ao inscrever para um evento se os “Termos e condições” não está selecionado;
- “ical calendar” removido no Month View;
- reCaptcha instalado na caixa de texto para impedir spam;
- Plugins actualizados;


 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

 208-02-26_2328
 1.1)
   
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

 2017-11-04_1244
 1.0)
 Versão 1.0 - com os serviços minimos a funcionar.
 
